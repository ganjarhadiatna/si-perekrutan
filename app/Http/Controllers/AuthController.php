<?php

namespace App\Http\Controllers;

use App\Karyawan;
use App\Jabatan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        session()->regenerate();
    }

    public function index()
    {
        return view('auth.login');
    }

    public function submit(Request $request)
    {
        // session()->regenerate();

        $this->validate($request, [
            'email' => 'required|max:255',
            'password' => 'required|min:6'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = Karyawan::CheckUser($email);

        if ($user)
        {
            $errors = ['email' => 'Maaf, email kamu salah'];
    
            return view('auth.login')->withErrors($errors);
        }

        if (Hash::check($password, $user->password))
        {
            session([
                'id' => $user->id,
                'email' => $user->email,
                'jabatan' => $user->jabatan,
                'divisi' => $user->divisi
            ]);

            $data = [
                'id' => session('id'),
                'email' => session('email'),
                'jabatan' => session('jabatan'),
                'divisi' => session('divisi')
            ];

            return view('admin.index', $user = $data);
        }
        else 
        {
            $errors = ['password' => 'Maaf, password kamu salah'];
    
            return view('auth.login')->withErrors($errors);
        }
    }
}
