<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Karyawan extends Model
{
    use Notifiable;
    
    protected $table = "karyawan";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'NIK', 'nama', 'email', 'password', 'no_telp', 'alamat', 'tanggal_masuk', 'status', 'id_jabatan'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeCheckUser($quer, $email)
    {
        return $this
        ->select(
            'karyawan.id',
            'karyawan.nama',
            'karyawan.email',
            'karyawan.status',
            'karyawan.password',
            'jabatan.nama as jabatan',
            'divisi.nama as divisi'
        )
        ->join('jabatan', 'jabatan.id', '=', 'karyawan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'jabatan.id_divisi')
        ->where('email', $email)
        ->first();
    }
}
