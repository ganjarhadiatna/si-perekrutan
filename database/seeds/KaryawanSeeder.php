<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'NIK' => 'SPL9905033662',
                'nama' => 'Gunawan',
                'email' => 'gunawan@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'no_telp' => '082221221221',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '10',
                'id_divisi' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'NIK' => 'SPL9905033234',
                'nama' => 'Bambang',
                'email' => 'bambang@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'no_telp' => '08522122444',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '4',
                'id_divisi' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'NIK' => 'SPL9905033999',
                'nama' => 'Surya',
                'email' => 'surya@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'no_telp' => '08522122555',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '7',
                'id_divisi' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('karyawan')->insert($data);
    }
}
