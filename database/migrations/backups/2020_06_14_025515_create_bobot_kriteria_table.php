<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBobotKriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bobot_kriteria', function (Blueprint $table) {
            $table->id();
            $table->double('bobot', 8, 2);
            $table->foreignId('id_kriteria');
            $table->foreignId('id_data_nilai');
            $table->timestamps();

            // modifier
            $table->foreign('id_kriteria')->references('id')->on('kriteria');
            $table->foreign('id_data_nilai')->references('id')->on('data_nilai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bobot_kriteria');
    }
}
