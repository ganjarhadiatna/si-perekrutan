<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianRekruitmenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_rekruitmen', function (Blueprint $table) {
            $table->id();
            $table->double('nilai', 8, 2);
            $table->foreignId('id_kriteria_rekruitmen');
            $table->foreignId('id_karyawan');
            $table->foreignId('id_pelamar');
            $table->timestamps();

            // modifier
            $table->foreign('id_kriteria_rekruitmen')->references('id')->on('kriteria_rekruitmen');
            $table->foreign('id_karyawan')->references('id')->on('karyawan');
            $table->foreign('id_pelamar')->references('id')->on('pelamar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_rekruitmen');
    }
}
