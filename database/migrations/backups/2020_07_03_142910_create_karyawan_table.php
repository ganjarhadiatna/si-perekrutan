<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->id();
            $table->string('NIK', 16);
            $table->string('nama', 150);
            $table->string('email', 150);
            $table->string('no_telp', 15);
            $table->string('alamat', 255);
            $table->date('tanggal_masuk');
            $table->enum('status', ['tetap', 'kontrak'])->default('kontrak');
            $table->foreignId('id_jabatan');
            $table->timestamps();

            // modifier
            $table->unique(['NIK', 'email']);
            $table->foreign('id_jabatan')->references('id')->on('jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
