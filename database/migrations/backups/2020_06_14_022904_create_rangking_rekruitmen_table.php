<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRangkingRekruitmenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_seleksi_rekreuitmen', function (Blueprint $table) {
            $table->id();
            $table->double('hasil', 8, 2);
            $table->foreignId('id_karyawan');
            $table->foreignId('id_pelamar');
            $table->timestamps();

            // modifier
            $table->unique(['id_karyawan', 'id_pelamar']);
            $table->foreign('id_karyawan')->references('id')->on('karyawan');
            $table->foreign('id_pelamar')->references('id')->on('pelamar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_seleksi_rekreuitmen');
    }
}
