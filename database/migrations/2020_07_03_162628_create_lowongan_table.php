<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLowonganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lowongan', function (Blueprint $table) {
            $table->id();
            $table->text('judul', 150);
            $table->text('deskripsi');
            $table->string('kuota', 32);
            $table->date('tanggal_dibuka');
            $table->date('tanggal_ditutup');
            $table->enum('status', ['dibuka', 'ditutup'])->default('dibuka');
            $table->foreignId('id_karyawan');
            $table->foreignId('id_jabatan');
            $table->timestamps();

            // modifier
            $table->foreign('id_karyawan')->references('id')->on('karyawan');
            $table->foreign('id_jabatan')->references('id')->on('jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lowongan');
    }
}
