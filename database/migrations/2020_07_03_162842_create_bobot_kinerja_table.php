<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBobotKinerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bobot_kinerja', function (Blueprint $table) {
            $table->id();
            $table->double('bobot', 8, 2);
            $table->foreignId('id_kriteria_kinerja');
            $table->foreignId('id_data_nilai');
            $table->timestamps();

            // modifier
            $table->foreign('id_kriteria_kinerja')->references('id')->on('kriteria_kinerja');
            $table->foreign('id_data_nilai')->references('id')->on('data_nilai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bobot_kinerja');
    }
}
